#!/bin/bash
cd ${0%/*} || exit 1    # run from this directory

# To use system version of a ThirdParty code, set the corresponding
# environmental variable: for example, for the Eigen library add the following
# line to your ~.bashrc:
#     export EIGEN_DIR="/opt/local/include/eigen3"

# Eigen library: http://eigen.tuxfamily.org/index.php?title=Main_Page
# Check if eigen3 directory is found
if [ -d $(pwd)/eigen3 ]
then
    echo "eigen3 found."
else
    if [ -z $EIGEN_DIR ]
    then
        # Download eigen using wget
        echo "Downloading eigen3"
        wget --no-check-certificate \
            https://gitlab.com/libeigen/eigen/-/archive/3.3.7/eigen-3.3.7.tar.gz
        if [ $? -ne 0 ]
        then
            echo "Download failed!"
            echo "Check your internet connection and check the following link "
            echo "is valid: https://gitlab.com/libeigen/eigen/-/archive/3.3.7/eigen-3.3.7.tar.gz"
            echo
            exit 1
        fi
        \tar xvf eigen-3.3.7.tar.gz
        \rm -f eigen-3.3.7.tar.gz
        \mv eigen-3.3.7 eigen3
        (cd eigen3 && wmakeLnInclude .)
    else
        #  If EIGEN_DIR is set then then we will create a symbolic link to it
        echo "EIGEN_DIR is set: creating symbolic link"
        \ln -s $EIGEN_DIR eigen3
    fi
fi
